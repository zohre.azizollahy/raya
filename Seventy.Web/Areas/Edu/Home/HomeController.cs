﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Seventy.DomainClass.EDU;
using Seventy.Service.EDU.CourseCategory;
using Seventy.Service.EDU.Lesson;
using Seventy.Service.Users;
using Seventy.ViewModel.EDU;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Seventy.ViewModel.EDU.Lesson;
using System;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Seventy.Web.Areas.Edu.Home
{
    [Area("Edu")]
    [Authorize(Policy = "user")]
    public class HomeController : Controller
    {
        private static IUserManager _um;
        private static ICourseCategoryService _ccs;
        private static AutoMapper.IMapper _mapper;
        private readonly ILessonService _lessonService;


        public HomeController(ICourseCategoryService ccs, IUserManager um, AutoMapper.IMapper mapper, Seventy.Service.EDU.Lesson.ILessonService lessonService)
        {
            _ccs = ccs;
            _um = um;
            _mapper = mapper;
            _lessonService = lessonService;
        }

        [Route("edu", Name = "edu")]
        public async Task<ActionResult> Index()
        {
            try
            {
                var lesson = await _lessonService.GetAllAsync();

                var lessonEditViewModels = Maping().Map<IEnumerable<Lesson>, IEnumerable<LessonEditViewModel>>(lesson);

                return View(lessonEditViewModels);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("edu/Home/Create", Name = "create")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edu/Home/Create", Name = "createNew")]
        public async Task<IActionResult> Create([Bind("Title,Description")] LessonEditViewModel lessonEditViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var newLesson = Maping().Map<LessonEditViewModel, Lesson>(lessonEditViewModel);
                    newLesson.RegUserID = _um.GetCurrentUserID();

                    if (await _lessonService.InsertAsync(newLesson) != null)
                        return RedirectToAction(nameof(Index));
                    
                }
                else
                {
                    return View(lessonEditViewModel);
                }

            }
            catch (Exception ex)
            {

            }
            return View(lessonEditViewModel);
           
        }


        [HttpGet]
        [Route("edu/Home/Edit/{id:int}", Name = "edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _lessonService.GetByIDAsync(id);

            if (lesson == null)
            {
                return NotFound();
            }
           
            var lessonViewModel = Maping().Map<Lesson, LessonEditViewModel>(lesson);

            return View(lessonViewModel);
        }

        [HttpPost]
        [Route("edu/Home/Edit/{id:int}", Name = "editConfirm")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Description")] LessonEditViewModel lessonViewModel)
        {
            if (id != lessonViewModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    var lesson = Maping().Map<LessonEditViewModel, Lesson>(lessonViewModel);

                    var lesson_1 = await _lessonService.GetByIDAsync(id);

                    lesson.RegUserID = lesson_1.RegUserID;
                    
                    await _lessonService.UpdateAsync(lesson);

                }
                catch (DbUpdateConcurrencyException)
                {

                    throw;

                }
                return RedirectToAction(nameof(Index));
            }
            return View(lessonViewModel);
        }


        [HttpGet]
        [Route("edu/Home/Delete/{id:int}", Name = "delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _lessonService.GetByIDAsync(id);

            if (lesson == null)
            {
                return NotFound();
            }

            return View(lesson);
        }

        [Route("edu/Home/Delete/{id:int}", Name = "deleteConfirm")]
        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var lesson = await _lessonService.GetByIDAsync(id);

            await _lessonService.DeleteAsync(lesson);
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Description")] Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                if (await _lessonService.InsertAsync(lesson) != null)
                    return RedirectToAction(nameof(Index));
            }
            return View(lesson);
        }

        private IMapper Maping()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Lesson, LessonEditViewModel>().ReverseMap();
            });

            IMapper iMapper = config.CreateMapper();
            return iMapper;
        }

    }
}