﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Seventy.Service.Core.Users;
using Seventy.Service.Users;

namespace Seventy.Web.Features.Home
{
    public class HomeController : Controller
    {
        private readonly INotif _notifManager;
        public HomeController(INotif notifManager)
        {
            _notifManager = notifManager;
            //TempData.Put("notifs", UserManager.getUserNotification());
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Test()
        {
            return View();
        }


        [Route("getNotifications")]
        public async Task<string> GetNotifications()
        {
            string st = "";

           // if (_NotifManager.GetCurrentUserAsync() != null)
            {
                var notifs = await _notifManager.getUserNotification();
                if (notifs == null) return st;
                foreach (var item in notifs)
                {
                    var type = "";
                    switch (item.MsgType)
                    {
                        case "1":
                            type = "ft-message-circle";break;
                        case "2":
                            type = "ft-lock"; break;
                        case "3":
                            type = "ft-life-buoy"; break;
                        default:
                            type = "ft-link-2"; break;
                    }
                     st += $"<a href=\"javascript:void(0)\">"+
                            $"<div class=\"media\">"+
                                $"<div class=\"media-left align-self-center\"><i class=\"ft-share info font-medium-4 mt-2\"></i></div>"+
                                $"<div class=\"media-body\">"+
                                    $"<h6 class=\"media-heading info\">{item.MsgTitle}</h6>"+
                                    $"<p class=\"notification-text font-small-3 text-muted text-bold-600\">{item.Description}!</p><small>"+
                                        $"<time class=\"media-meta text-muted\" datetime=\"{item.RegDate}\">{Helper.GetPersianDate(item.RegDate)}</time>"+
                                    $"</small>"+
                                $"</div>"+
                            $"</div>"+
                        $"</a>";
                }                
            }
            return st;
        }
    }
}