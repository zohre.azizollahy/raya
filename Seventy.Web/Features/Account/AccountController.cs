﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Seventy.Service.Core.Messenger;
using Seventy.Service.Users;
using Seventy.ViewModel.Core.Users;

namespace Seventy.Web.Features.Account
{
    public class AccountController : Controller
    {
        private readonly IUserManager _userManager;
        private readonly IMessageSender _smsService;

        public AccountController(IUserManager userManager, IMessageSender smsService)
        {
            _userManager = userManager;
            _smsService = smsService;
        }


        [HttpGet, AllowAnonymous, Route("login", Name = "Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        [Route("login")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var loginResult = await _userManager.Login(model);
            switch (loginResult)
            {
                case Service.Core.Users.LoginResult.Success:
                    var user = await _userManager.FindByMobileAsync(model.Mobile);
                    var cookieClaims = CreateCookieClaimsAsync(user);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, cookieClaims, new Microsoft.AspNetCore.Authentication.AuthenticationProperties { IsPersistent = true, IssuedUtc = DateTimeOffset.UtcNow, ExpiresUtc = DateTimeOffset.UtcNow.AddDays(10) });

                    // todo goto witch page ?
                    return RedirectToRoute("Edu");
                case Service.Core.Users.LoginResult.Error:
                case Service.Core.Users.LoginResult.Disable:
                    ViewData["err"] = "اکانت شما غیر فعال است";
                    return View(model);
                case Service.Core.Users.LoginResult.NotExist:
                    ViewData["err"] = "نام کاربری و رمز عبور مطابقت ندارد";
                    return View(model);
            }
            return View(model);
        }

        [HttpGet, AllowAnonymous, Route("Register", Name = "Register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterPost(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Register), model);
            }

            if (!HttpContext.Session.Keys.Contains(model.Mobile))
            {
                ModelState.AddModelError("ActivationCode", "کد فعال سازی را وارد نمایید");
                return RedirectToAction("Register");
            }
            else if (HttpContext.Session.GetString(model.Mobile) != model.UserActivationCode)
            {
                ModelState.AddModelError("ActivationCode", "کد فعال سازی اشتباه");
                return RedirectToAction("Register");
            }
            var registerResult = await _userManager.Register(model);
            switch (registerResult)
            {
                case Service.Core.Users.RegisterResult.Success:
                    {
                        HttpContext.Session.Remove(model.Mobile);
                        return RedirectToAction("/edu");
                    }
                case Service.Core.Users.RegisterResult.Error:
                    ModelState.AddModelError("UserName", "");
                    return RedirectToAction("Register");
                case Service.Core.Users.RegisterResult.UserExist:
                    {
                        HttpContext.Session.Remove(model.Mobile);
                        return RedirectToAction(nameof(Login));
                    }
            }
            return View(model);
        }

        [HttpPost, AllowAnonymous, Route("GenerateActivationCode", Name = "GenerateActivationCode")]
        public async Task<bool> GenerateActivationCode(string mobile)
        {
            if (string.IsNullOrEmpty(mobile))
                return false;

            mobile = mobile.Trim();

            var c = new Random().Next(111111, 999999).ToString();
            HttpContext.Session.SetString(mobile, c);

            AbstractMessage shortMessage = new ShortMessage(new SMSMessenger());
            var result = await shortMessage.Verify(c, mobile);

            if (!result.Sent)
            {
                Console.WriteLine(result.Message);
                return false;
            }
            else
            {
                return true;
            }

        }

        [HttpGet, AllowAnonymous, Route("ForgetPass", Name = "ForgetPass")]
        public IActionResult ForgetPass()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgetPassPost(LoginViewModel model)
        {
            var forgetResult = await _userManager.ForgetPassword(model.Mobile);
            switch (forgetResult)
            {
                case Service.Core.Users.LoginResult.Success:
                    return RedirectToAction("Login", new { success = "رمز جدید به موبایل شما ارسال شد " });
                case Service.Core.Users.LoginResult.Error:
                    ModelState.AddModelError("Mobile", "خطایی رخ داده است");
                    return View("ForgetPass");
                case Service.Core.Users.LoginResult.Disable:
                    ModelState.AddModelError("Mobile", "اکانت شما غیر فعال است");
                    return View("ForgetPass");
                case Service.Core.Users.LoginResult.NotExist:
                    ModelState.AddModelError("Mobile", "شماره موبایل شما در سیستم یافت نشد");
                    return View("ForgetPass");
                default:
                    break;
            }
            return View(model);
        }

        [Route("logout")]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account", null);
        }
        private ClaimsPrincipal CreateCookieClaimsAsync(DomainClass.Core.Users user)
        {
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Mobile));
            identity.AddClaim(new Claim("DisplayName", user.Mobile));

            // custom data
            identity.AddClaim(new Claim(ClaimTypes.UserData, user.ID.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
            //if (user == (byte)RoleUser.User)
            //    identity.AddClaim(new Claim(ClaimTypes.Role, RoleUser.User.ToString()));
            //else
            //    identity.AddClaim(new Claim(ClaimTypes.Role, RoleUser.Admin.ToString()));
             // }
            return new ClaimsPrincipal(identity);
        }


    }
}