﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Seventy.Service.Users;
using System.Threading.Tasks;

namespace Seventy.Web.StartupCustomizations.Filter
{
    public class AuthenticationFilter : IAsyncActionFilter
    {
        private readonly IUserManager _userManager;

        public AuthenticationFilter(IUserManager userManager)
        {
            _userManager = userManager;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated == false || await _userManager.GetCurrentUserAsync() == null)
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Account" }, { "action", "AccessDenied" } });
            }
            else
            {
                await next();
            }
        }
    }

}
